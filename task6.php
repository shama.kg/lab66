<?php
$array = [-17, -12, -7, -2];

function progress($array): int|null
{
    $d = null;
    for ($i = 0 ; $i < count($array); $i++){
        if (array_key_exists($i+1, $array  )){
            $res = $array[$i+1] - $array[$i];
            if ($d == null){
                $d = $res;
            }
            if($d != $res){
                return null;
            }
        }
    }
    return $d;
}
var_dump(progress($array));



